package ca.sheridancollege.maven_project;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int num1 = 1;
        int num2 = 2;
        int result = add(num1, num2);
        System.out.printf("The total is: %d", result);
    }
    
    public static int add(int num1, int num2) {
    	int result = num1 + num2;
    	return result;
    }
}
